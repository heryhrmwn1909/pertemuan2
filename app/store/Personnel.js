Ext.define('Pertemuan.store.Personnel', {
    extend: 'Ext.data.Store',
    storeId: 'personnel',
    alias: 'store.personnel',

    fields: [
        'photo', 'npm', 'name', 'email', 'phone','birthday'
    ],

    data: { items: [
        {photo: '<img src="resources/image/HeryHermawan.jpg" width="100px" height="100px">' ,npm: '183510006', name: 'Hery Hermawan', email: "heryhermawan@student.uir.ac.id", phone: "082233493936", birthday: "1999-18-09"},
        {photo: '<img src="resources/image/SuryaEfendi.jpg" width="100px" height="100px">' ,npm: '183510034', name: 'Surya Efendi',  email: "suryaefendi@student.uir.ac.id",  phone: "078068068877", birthday: "1998-29-01" },
        {photo: '<img src="resources/image/Agung.jpg" width="100px" height="100px">' ,npm: '183510009', name: 'Agung Saputra',  email: "agungsaputra@student.uir.ac.id",    phone: "44565768678", birthday: "1994-23-04" },
        {photo: '<img src="resources/image/Herufai.jpg" width="100px" height="100px">' ,npm: '183510030', name: 'Heru Rifai',  email: "herufai@student.uir.ac.id",       phone: "56546576576", birthday: "1991-11-09" },
    ]},

    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});