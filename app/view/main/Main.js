/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Pertemuan.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',

        'Pertemuan.view.main.MainController',
        'Pertemuan.view.main.MainModel',
        'Pertemuan.view.main.List',
        'Pertemuan.view.main.User',
        'Pertemuan.view.main.Carousel',
        'Pertemuan.view.main.Toolbars',
        'Pertemuan.view.main.BasicDataView',
        'Pertemuan.view.form.Login',
        'Pertemuan.view.main.Chart',
        'Pertemuan.view.main.Area',
        'Pertemuan.view.tree.TreeList'


        
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },

    tabBarPosition: 'bottom',

    items: [
        {
            title: 'Home',
            iconCls: 'x-fa fa-home',
            layout: 'fit',
            // The following grid shares a store with the classic version's grid as well!
            items: [{
                xtype: 'mainlist'
            }]
        },{
            title: 'Users',
            iconCls: 'x-fa fa-user',
            layout: 'fit',
            items: [{
                xtype: 'huser'
            }]
        },{
            title: 'Groups',
            iconCls: 'x-fa fa-users',
            layout: 'fit',
            items: [{
                xtype: 'hcarousel'
            }]
        },{
            title: 'Settings',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'hdata'
            }]
        },{
            title: 'Chart1',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'hchart'
            }]
        },{
            title: 'Chart2',
            iconCls: 'x-fa fa-cog',
            layout: 'fit',
            items: [{
                xtype: 'harea'
            }]
        },{
            title: 'Tree',
            iconCls: 'x-fa fa-tree',
            layout: 'fit',
            items: [{
                xtype: 'htree-panel'
            }]
        }
    ]
});
