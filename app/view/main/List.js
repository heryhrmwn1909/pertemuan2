/**
 * This view is an example list of people.
 */
Ext.define('Pertemuan.view.main.List', {
    extend: 'Ext.grid.Grid',
    xtype: 'mainlist',

    requires: [
        'Pertemuan.store.Personnel'
    ],

    title: 'Personnel',

    store: {
        type: 'personnel'
    },

    columns: [
        { text: 'Npm',  dataIndex: 'npm', width: 110 },
        { text: 'Name',  dataIndex: 'name', width: 160 },
        { text: 'Email', dataIndex: 'email', width: 230 },
        { text: 'Phone', dataIndex: 'phone', width: 150 },
        { text: 'birthday', dataIndex: 'birthday', width: 150 }
    ],

    listeners: {
        select: 'onItemSelected'
    }
});
