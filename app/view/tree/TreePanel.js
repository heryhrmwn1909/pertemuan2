Ext.define('Pertemuan.view.tree.TreePanel', {
    extend: 'Ext.Container',
    xtype: 'htree-panel',
    requires: [
        'Ext.layout.HBox',
        'Pertemuan.view.tree.TreeList'
    ],
    
    layout: {
        type: 'hbox',
        pack: 'center',
        align: 'stretch'
    },
    margin: '0 10',
    defaults: {
        margin: '0 0 10 0',
        bodypadding: 10
    },
    items: [
        {
            xtype: 'htree-list',
            flex: 1
        },
        {
            xtype: 'panel',
            id: 'detailtree',
            flex: 1,
            html: 'belum ada terpilih'
        }
    ]
})